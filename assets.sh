#/bin/bash

mkdir -p build

###############
#   Favicon   #
###############
cp -ru src/favicon build

###########
#   CSS   #
###########
mkdir -p build/css
cp -ru src/css build
sass-rs --sass --compressed < src/style.sass > build/css/style.css

##############
#   Images   #
##############
cp -ru src/imgs build

#############
#   Dtops   #
#############
rm -rf build/imgs/dtops
cp -ru src/imgs/dtops build/imgs
cd build/imgs/dtops

for file in *
do
    convert -sample 400x225 $file $(basename -s .png $file)_tmb.png
done

cd ../../..

################
#   Memories   #
################
rm -rf build/imgs/memories
cp -ru src/imgs/memories build/imgs
cd build/imgs/memories

for file in *
do
    convert -sample 150x100 $file $(basename -s .png $file)_tmb.png
done

cd ../../..
