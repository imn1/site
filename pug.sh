#!/bin/bash

#############
#   Pages   #
#############
# Converts all pages (*.pug) directly in "build" folder.
for f in $(find src -maxdepth 1 -name "*.pug" ! -name "_*")
do
    echo -n "Converting $f ..."
    pypugjs $f build/$(basename -s .pug build/$f).html
done

############
#   Blog   #
############
mkdir -p build/blog

# Converts all pages (*.pug) in "build/blog" folder.
for f in $(find src/blog -name "*.pug")
do
    echo -n "Converting $f ..."
    pypugjs $f build/blog/$(basename -s .pug build/blog/$f).html
done
